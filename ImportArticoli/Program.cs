﻿using Excel;
using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;


namespace ImportArticoli
{
    class Program
    {
        public static string DataEla;
        public static string OraEla;
        private XmlDocument docXml = new XmlDocument();
        private SAPbobsCOM.Company oCompany;
        private SAPbobsCOM.SBObob oSBObob;
        private SAPbobsCOM.Recordset oRecordset;
        private SAPbobsCOM.Recordset oRecordset2;
        private SAPbobsCOM.Items oItem;
        private SAPbobsCOM.Documents oOIGN;
        private SAPbobsCOM.SpecialPrices oSPE;
        private Dictionary<string, string> DicCaratteriSpeciali;

        static void Main(string[] args)
        {
            var prog = new Program();

            //apro il file xml contenente i prametri di connessione
            string path = AppDomain.CurrentDomain.BaseDirectory;
            //string path2 = System.Environment.CurrentDirectory;

            prog.docXml.Load(path + "\\parametri.xml");
            prog.Log("Avvio Importazioni Articoli " + DateTime.Now.ToString());
            prog.connectionSAP(prog.docXml);
            prog.ReadDir(prog.docXml);
            prog.disconnectionSAP();

        }

        public void Log(string logMessage)
        {
            DateTime dt = new DateTime();
            dt = DateTime.Now;

            string logFile = "c:/temp/LogImportazioniFile_" + dt.ToString("yyyyMMdd") + ".txt";

            using (StreamWriter w = File.AppendText(logFile))
            {

                w.WriteLine(logMessage);
            }
            Console.WriteLine(logMessage);
        }

        private void connectionSAP(XmlDocument docXml)
        {
            oCompany = new SAPbobsCOM.Company();

            //Leggo i parametri di connessione dal file xml
            XmlNodeList elemList = docXml.GetElementsByTagName("DB");
            foreach (XmlNode node in elemList)
            {
                oCompany.Server = node.Attributes["Server"].Value;
                //oCompany.LicenseServer = node.Attributes["License"].Value;
                oCompany.DbUserName = node.Attributes["DbUserName"].Value;
                oCompany.DbPassword = node.Attributes["DbPassword"].Value;
                oCompany.CompanyDB = node.Attributes["CompanyDB"].Value;
                oCompany.UserName = node.Attributes["UserName"].Value;
                oCompany.Password = node.Attributes["Password"].Value;
                switch (node.Attributes["DBType"].Value)
                {
                    case "SQL19":
                        oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2019;
                        break;

                    case "SQL17":
                        oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2017;
                        break;

                    case "SQL16":
                        oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2016;
                        break;

                    case "SQL14":
                        oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
                        break;

                    case "SQL12":
                        oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012;
                        break;

                    case "HANA":
                        oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;
                        break;

                    default:
                        oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;
                        break;
                }

            }

            oCompany.UseTrusted = false;

            int errCode = oCompany.Connect();

            if (errCode != 0)
            {
                string err;
                int code;
                oCompany.GetLastError(out code, out err);
                Log("ERRORE, CONNESSIONE A SAP NON RIUSCITA errore N: " + code + "             " + err);
                return;
            }
            else
            {
                Log("Connesso a SAP");
            }
            oSBObob = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoBridge);
        }

        private void disconnectionSAP()
        {
            oCompany.Disconnect();
            oCompany = null;
        }

        private void ReadDir(XmlDocument docXml)
        {
            XmlNodeList elemList = docXml.GetElementsByTagName("DB");
            foreach (XmlNode node in elemList)
            {
                if (Directory.Exists(node.Attributes["Path"].Value))
                {
                    Log("Trovati " + Directory.GetFiles(node.Attributes["Path"].Value).Length+ " files");
                    foreach (string file in Directory.GetFiles(node.Attributes["Path"].Value))
                    {
                        FileInfo fileInfo = new FileInfo(file);

                        string fileName = fileInfo.Name;
                        string pathfile = fileInfo.ToString();

                        ReadFile(pathfile.Replace("\\", @"\"));
                        SpostaFile(pathfile.Replace("\\", @"\"), node.Attributes["Path"].Value);
                        GenFileExport(node.Attributes["PathExport"].Value);
                    }
                }
            }

        }

        private void SpostaFile(string PathFile, string Path)
        {
            string pathDest = Path + "ELA\\";
            if (!Directory.Exists(pathDest))
            {
                Directory.CreateDirectory(pathDest);
            }
            string fileOri = PathFile;
            string fileDest = pathDest + DateTime.Now.ToString("yyyyMMddHHmmss") + "File_Elaborato.csv";

            File.Move(fileOri, fileDest);
        }

        private void ReadFile(string PathFile)
        {
            Log("Elaborazione file " + PathFile);
            int riga = 0;
            ArrayList Articoli = new ArrayList();
            ArrayList QtaArt = new ArrayList();
            ArrayList MagazzinoArt = new ArrayList();
            StreamReader reader = new StreamReader(File.OpenRead(PathFile), Encoding.GetEncoding("iso-8859-1"));// Encoding.iso);

            //Prendo i caratteri speciali
            //popolaDicCaratteriSpeciali();

            List<string> listA = new List<string>();
            //Ciclo sulle righe e popolo le variabili
            while (!reader.EndOfStream)
            {
                if (riga > 1)
                {
                    string CardCode = "";
                    string Season = "";
                    string Season2 = "";
                    string ItemCode = "";
                    string BarCode = "";
                    string Marchio = "";
                    string Linea = "";
                    string Settore = "";
                    string Modello = "";
                    string Tessuto = "";
                    string Drop = "";
                    string Colore = "";
                    string Taglia = "";
                    string NomeSito = "";
                    string ItemName = "";
                    string Composizione = "";
                    string Lavaggio = "";
                    string Qta = "";
                    string Prezzo = "";
                    string Valuta = "";
                    string Bozza = "";
                    string Peso = "";
                    string Collezione = "";
                    string CodiceMerceologico = "";
                    string importare = "";
                    string URLImmagine1 = "";
                    string URLImmagine2 = "";
                    string URLImmagine3 = "";
                    string URLImmagine4 = "";
                    string URLImmagine5 = "";
                    string URLImmagine6 = "";
                    string URLImmagine7 = "";
                    string URLImmagine8 = "";
                    string Category = "";
                    string PrezzoScontato = "";
                    string Magazzino = "";

                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    int col = 0;
                    //Ciclo sulle colonne
                    foreach (var item in values)
                    {
                        switch (col)
                        {
                            case 0: //Colonna A - Codice Cliente
                                CardCode = item.Replace("\"", "");
                                break;
                            case 1: //Colonna B - Stagione di Vendita
                                Season = item.Replace("\"", "");
                                break;
                            case 2: //Colonna C - Stagione del modello
                                Season2 = item.Replace("\"", "");
                                break;
                            case 3: //Colonna D - Codice Articolo
                                ItemCode = item.Replace("\"", "");
                                break;
                            case 4: //Colonna E - Codice a Barre
                                BarCode = item.Replace("\"", "");
                                break;
                            case 5: //Colonna F - Marchio
                                Marchio = item.Replace("\"", "");
                                break;
                            case 6: //Colonna G - Linea
                                Linea = item.Replace("\"", "");
                                break;
                            case 7: //Colonna H - Settore
                                Settore = item.Replace("\"", "");
                                break;
                            case 8: //Colonna I - Modello
                                Modello = item.Replace("\"", "");
                                break;
                            case 9: //Colonna J - Tessuto
                                Tessuto = item.Replace("\"", "");
                                break;
                            case 10: //Colonna K - Drop
                                Drop = item.Replace("\"", "");
                                break;
                            case 11: //Colonna L - Colore
                                Colore = item.Replace("\"", "");
                                break;
                            case 12: //Colonna M - Taglia
                                Taglia = item.Replace("\"", "");
                                break;
                            case 13: //Colonna N - NomeSito
                                NomeSito = item.Replace("\"", "");
                                break;
                            case 14: //Colonna O - Descrizione Articolo
                                ItemName = item.Replace("\"", "");
                                break;
                            case 15: //Colonna P - Composizione
                                Composizione = item.Replace("\"", "");
                                break;
                            case 16: //Colonna Q - lavaggio
                                Lavaggio = item.Replace("\"", "");
                                break;
                            case 17: //Colonna R - Quantità
                                Qta = item.Replace(".", ",");
                                break;
                            case 18: //Colonna S - Prezzo Scontato
                                PrezzoScontato = item.Replace(".", ",");
                                break;
                            case 19: //Colonna T - Valuta
                                Valuta = item.Replace("\"", "");
                                break;
                            case 20: //Colonna U - Bozza
                                Bozza = item.Replace("\"", "");
                                break;
                            case 21: //Colonna V - Peso
                                Peso = item.Replace("\"", "");
                                break;
                            case 22: //Colonna W - Collezione
                                Collezione = item.Replace("\"", "");
                                break;
                            case 23: //Colonna X - CodiceMerceologico
                                CodiceMerceologico = item.Replace("\"", "");
                                break;
                            case 25: //Colonna Z - Prezzo pieno
                                Prezzo = item.Replace(".", ",");
                                break;
                            case 26: //Colonna AA- Category
                                Category = item.Replace("\"", "");
                                break;
                            case 27: //Colonna AB - Magazzino
                                Magazzino = item.Replace("\"", "");
                                break;

                            case 28: //Colonna AC - URL Immagine 1
                                URLImmagine1 = item.Replace("\"", "");
                                break;
                            case 29: //Colonna AD - URL Immagine 2
                                URLImmagine2 = item.Replace("\"", "");
                                break;
                            case 30: //Colonna AE - URL Immagine 3
                                URLImmagine3 = item.Replace("\"", "");
                                break;
                            case 31: //Colonna AF - URL Immagine 4
                                URLImmagine4 = item.Replace("\"", "");
                                break;
                            case 32: //Colonna AG - URL Immagine 5
                                URLImmagine5 = item.Replace("\"", "");
                                break;
                            case 33: //Colonna AH - URL Immagine 6
                                URLImmagine6 = item.Replace("\"", "");
                                break;
                            case 34: //Colonna AI - URL Immagine 7
                                URLImmagine7 = item.Replace("\"", "");
                                break;
                            case 35: //Colonna AJ - URL Immagine 8
                                URLImmagine8 = item.Replace("\"", "");
                                break;

                                /* case 24: //Colonna Z - importare
                                     importare = item.Replace("\"", "");
                                     break; */
                        }
                        col++;
                    }
                    int res = 0;
                    int res2 = 0;

                    /*
                    if (importare == "NO" || importare == "No" || importare == "N")
                    {
                        Log("Colonna di Importazione =" + importare +" => riga "+ riga +" non importata.");
                        oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                        string s = Drop +  " " + Modello + " " + Colore + " ;";
                        string cols = "( col1, col2, col3 )";
                        oRecordset.DoQuery("INSERT INTO \"@FO_EC_CODCAR\" T0 " + cols + " VALUES " + s);
                        riga++;
                        continue;
                    }
                    */

                    //Campo usato per le importazioni Bozza a NO importo, Bozza a YES Non importo
                    if (Bozza == "NO" || Bozza == "No" || Bozza == "N")
                    {
                        //Se ho almeno uno di questi campi restituisco errore
                        if (ItemCode == "" || Modello == "" || Taglia == "")
                        {
                            Log("ERRORE, Codice Articolo, Modello o Taglia Assente, controllare il file di input");
                        }
                        else
                        {


                            //Mi salvo le variabili su degli array per portarle ai metodi di creazione degli oggetti
                            string[] VarArticoli = { Season, Season2, ItemCode, BarCode, Marchio, Linea, Settore, Modello, Tessuto,
                                                    Drop, Colore, Taglia, NomeSito, ItemName, Composizione, Lavaggio, Peso, Collezione,
                                                    CodiceMerceologico, URLImmagine1, URLImmagine2, URLImmagine3, URLImmagine4, URLImmagine5,
                                                    URLImmagine6, URLImmagine7, URLImmagine8, Category, CardCode};

                            //Creo Articolo
                            Log("Inizio elaborazione articolo " + ItemCode);
                            res = CreaArt(VarArticoli);
                            //Se va bene popolo array entrata merci

                            //CONVERSIONI
                            double Qta2 = 0;
                            if (Qta != "")
                            {
                                Qta2 = double.Parse(Qta);
                            }
                            else
                            {
                                Qta2 = 0;
                            }
                            //
                            double Prezzo2 = 0;
                            if (Prezzo != "")
                            {
                                Prezzo2 = double.Parse(Prezzo);
                            }
                            else
                            {
                                Prezzo2 = 0;
                            }

                            //
                            double PrezzoScontato2 = 0;
                            if (PrezzoScontato != "")
                            {
                                PrezzoScontato2 = double.Parse(PrezzoScontato);
                            }
                            else
                            {
                                PrezzoScontato2 = 0;
                            }

                            if (Qta2 > 0 && res == 0)
                            {
                                //Popolo gli array per generare l'entrata merci successivamente
                                Articoli.Add(ItemCode);
                                QtaArt.Add(Qta);
                                MagazzinoArt.Add(Magazzino)
;                            }
                            //Se va bene genero prezzi speciali per bp
                            if (PrezzoScontato2 > 0 && res == 0)
                            {
                                string[] VarListini = { CardCode, ItemCode, PrezzoScontato, Valuta, Prezzo };
                                res2 = CreaListino(VarListini);
                            }
                        }
                    }
                }
                riga++;
            }
            //Genero entrata merci massiva
            int res3 = CreaEntrataMerci(Articoli, QtaArt, MagazzinoArt);

            //Chiudo il reader csv
            reader.Close();
            #region commentato
            /* 
         foreach (var worksheet in Workbook.Worksheets(@"C:\Users\user12\Desktop\Incom\File\test.xlsx"))
         {
             //Ciclo su tutte le righe
             foreach (var row in worksheet.Rows)
             {
                 //Salto la prima riga di testata 
                 if (riga > 0)
                 {


                     if (riga <= worksheet.Rows.Count() - 1)
                     {
                         //Per ogni riga, ciclo su tutte le colonne
                         foreach (var cell in row.Cells)
                         {
                             //Leggo i valori delle celle
                             switch (cell.ColumnIndex)
                             {
                                 case 0: //Colonna A - Codice Cliente
                                     CardCode = cell.Text.Replace(".", "");
                                     break;
                                 case 1: //Colonna B - Stagione di Vendita
                                     Season = cell.Text.Replace(".", "");
                                     break;
                                 case 2: //Colonna C - Stagione del modello
                                     Season2 = cell.Text.Replace(".", "");
                                     break;
                                 case 3: //Colonna D - Codice Articolo
                                     ItemCode = cell.Text.Replace(".", "");
                                     break;
                                 case 4: //Colonna E - Codice a Barre
                                     BarCode = cell.Text.Replace(".", "");
                                     break;
                                 case 5: //Colonna F - Marchio
                                     Marchio = cell.Text.Replace(".", "");
                                     break;
                                 case 6: //Colonna G - Linea
                                     Linea = cell.Text.Replace(".", "");
                                     break;
                                 case 7: //Colonna H - Settore
                                     Settore = cell.Text.Replace(".", "");
                                     break;
                                 case 8: //Colonna I - Modello
                                     Modello = cell.Text.Replace(".", "");
                                     break;
                                 case 9: //Colonna J - Tessuto
                                     Tessuto = cell.Text.Replace(".", "");
                                     break;
                                 case 10: //Colonna J - Drop
                                     Drop = cell.Text.Replace(".", "");
                                     break;
                                 case 11: //Colonna K - Colore
                                     Colore = cell.Text.Replace(".", "");
                                     break;
                                 case 12: //Colonna M - Taglia
                                     Taglia = cell.Text.Replace(".", "");
                                     break;
                                 case 13: //Colonna N - NomeSito
                                     NomeSito = cell.Text.Replace(".", "");
                                     break;
                                 case 14: //Colonna O - Descrizione Articolo
                                     ItemName = cell.Text.Replace(".", "");
                                     break;
                                 case 15: //Colonna P - Composizione
                                     Composizione = cell.Text.Replace(".", "");
                                     break;
                                 case 16: //Colonna Q - lavaggio
                                     Lavaggio = cell.Text.Replace(".", "");
                                     break;
                                 case 17: //Colonna R - Quantità
                                     Qta = cell.Text.Replace(".", "");
                                     break;
                                 case 18: //Colonna S - Prezzo Cliente
                                     Prezzo = cell.Text.Replace(".", "");
                                     break;
                                 case 19: //Colonna T - Valuta
                                     Valuta = cell.Text.Replace(".", "");
                                     break;
                                 case 20: //Colonna U - Bozza
                                     Bozza = cell.Text.Replace(".", "");
                                     break;
                             }
                         }
                         int res = 0;
                         int res2 = 0;
                         if (Bozza == "NO")
                         {
                             if (ItemCode == "" || Modello == "" || Taglia == "")
                             {
                                 //Errore
                             }
                             else
                             {
                                 string[] VarArticoli = { Season, Season2, ItemCode, BarCode, Marchio, Linea, Settore, Modello, Tessuto, Drop, Colore, Taglia, NomeSito, ItemName, Composizione, Lavaggio };
                                 res = CreaArt(VarArticoli);
                                 if (Qta != "" && res == 0)
                                 {
                                     string[] VarOGIN = { ItemCode, Qta };
                                     res2 = CreaEntrataMerci(VarOGIN);
                                 }
                                 if (Prezzo != "" && res == 0)
                                 {
                                     string[] VarListini = { CardCode, ItemCode, Prezzo, Valuta };
                                     res2 = CreaListino(VarListini);
                                 }
                             }
                         }
                     }
                 }
                 riga = riga + 1;
             }
         }
         */
            #endregion
        }

        private void popolaDicCaratteriSpeciali()
        {
            
            oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            oRecordset.DoQuery("SELECT * FROM \"@FO_EC_CODCAR\"");
            if (oRecordset.RecordCount == 0)
            {
                Log("Attenzione: nessun valore nella tabella dei caratteri speciali @FO_EC_CODCAR");
                return;
            }
            DicCaratteriSpeciali = new Dictionary<string, string>();
            while (oRecordset.BoF)
            {
                DicCaratteriSpeciali.Add(oRecordset.Fields.Item("U_FO_EC_CARORIG").Value, oRecordset.Fields.Item("U_FO_EC_CARSOST").Value);
                oRecordset.MoveNext();
            }
        }

        private string SostituisciCaratteriSpeciali(string s)
        {
            if (DicCaratteriSpeciali == null || DicCaratteriSpeciali.Count == 0) return s;

            foreach (var kvp in DicCaratteriSpeciali)
            {
                s = s.Replace(kvp.Key, kvp.Value);
            }
            return s;
        }

        private int CreaArt(string[] VarArticoli)
        {
            int res = 0;
            int error = 0;
            bool Nuovo = false;
            oItem = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
            oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordset2 = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            string ItemCode = VarArticoli[2];
            oRecordset.DoQuery("SELECT \"ItemCode\" FROM OITM WHERE \"ItemCode\" = '" + ItemCode + "'");
            if (oRecordset.RecordCount == 0)
            {
                oItem.ItemCode = ItemCode;
                //ArticolVendite = NO se è nuovo => articolo in "bozza"
                oItem.SalesItem = SAPbobsCOM.BoYesNoEnum.tNO;
                Nuovo = true;
            }
            else
            {
                oItem.GetByKey(ItemCode);
            }

            string CardCode = VarArticoli[28];
            oRecordset.DoQuery("SELECT U_FO_EC_ARTVEND FROM \"@FO_EC_DETNEGOZIO\" WHERE U_FO_EC_CARDCODE = '" + CardCode + "'");
            if (oRecordset.Fields.Item(0).Value == "N")
            {
                oItem.SalesItem = SAPbobsCOM.BoYesNoEnum.tNO;
            }
            else
            {
                oItem.SalesItem = SAPbobsCOM.BoYesNoEnum.tYES;
            }
            
            if (VarArticoli[12].Length > 199)
            {
                oItem.ItemName = VarArticoli[12].Substring(0, 198);
            }
            else
            {
                oItem.ItemName = VarArticoli[12];
            }
            oItem.BarCode = VarArticoli[3];

            //Immagini
            if (VarArticoli[19] != null && VarArticoli[19] != "")
            {
                oItem.UserFields.Fields.Item("U_FO_EC_IMG1").Value = VarArticoli[19];
            }
            if (VarArticoli[20] != null && VarArticoli[20] != "")
            {
                oItem.UserFields.Fields.Item("U_FO_EC_IMG2").Value = VarArticoli[20];
            }
            if (VarArticoli[21] != null && VarArticoli[21] != "")
            {
                oItem.UserFields.Fields.Item("U_FO_EC_IMG3").Value = VarArticoli[21];
            }
            if (VarArticoli[22] != null && VarArticoli[22] != "")
            {
                oItem.UserFields.Fields.Item("U_FO_EC_IMG4").Value = VarArticoli[22];
            }
            if (VarArticoli[23] != null && VarArticoli[23] != "")
            {
                oItem.UserFields.Fields.Item("U_FO_EC_IMG5").Value = VarArticoli[23];
            }
            if (VarArticoli[24] != null && VarArticoli[24] != "")
            {
                oItem.UserFields.Fields.Item("U_FO_EC_IMG6").Value = VarArticoli[24];
            }
            if (VarArticoli[25] != null && VarArticoli[25] != "")
            {
                oItem.UserFields.Fields.Item("U_FO_EC_IMG7").Value = VarArticoli[25];
            }
            if (VarArticoli[26] != null && VarArticoli[26] != "")
            {
                oItem.UserFields.Fields.Item("U_FO_EC_IMG8").Value = VarArticoli[26];
            }

            //Category
            
            if (VarArticoli[27] != null && VarArticoli[27] != "")
            {
                oItem.UserFields.Fields.Item("U_FO_EC_CATEGORY").Value = VarArticoli[27];
            }

            //Codice Merciologico
            oItem.IntrastatExtension.IntrastatRelevant = SAPbobsCOM.BoYesNoEnum.tYES;
            if (VarArticoli[18] != null && VarArticoli[18] != "")
            {
                oRecordset2.DoQuery("SELECT \"AbsEntry\" FROM \"ODCI\" WHERE \"Code\" = '" + VarArticoli[18] + "' AND \"ConfType\" = 'C' ");
                if (oRecordset2.RecordCount == 0)
                {
                    error = 1;
                    Log("ERRORE, Codice Intrastat " + VarArticoli[18] + " non trovato nella tabella di ODCI");
                }
                oItem.IntrastatExtension.CommodityCode = oRecordset2.Fields.Item(0).Value;
            }


            if (VarArticoli[7] == "")
            {
                Log("ATTENZIONE, SKU assente per l'articolo " + ItemCode);
            }
            //if (VarArticoli[4] == "50")
            //{
                oItem.UserFields.Fields.Item("U_FO_EC_VENDOR").Value = "U.S. Polo Assn.";
            //}

            oItem.UserFields.Fields.Item("U_FO_EC_SKU").Value = VarArticoli[7];
            oItem.UserFields.Fields.Item("U_FO_STAGIONE_VENDITA").Value = VarArticoli[0]; //Collezione ------
            oItem.UserFields.Fields.Item("U_FO_STAGIONE_SVILUPPO").Value = VarArticoli[1]; //No  -------
            //oItem.UserFields.Fields.Item("U_FO_EC_BRAND").Value = VarArticoli[4];
            oItem.UserFields.Fields.Item("U_FO_LINEA").Value = VarArticoli[5];//no -------
            oItem.UserFields.Fields.Item("U_FO_MARCHIO").Value = VarArticoli[4];//no    -------------
            oItem.UserFields.Fields.Item("U_FO_SETTORE").Value = VarArticoli[6];//no       ----------
            oItem.UserFields.Fields.Item("U_FO_TESSUTO").Value = VarArticoli[8];//no     ----------
            oItem.UserFields.Fields.Item("U_FO_EC_DROP").Value = VarArticoli[9];
            //oRecordset2.DoQuery("SELECT \"Code\" FROM \"@FO_EC_COLORI_FILATO\" WHERE \"U_FO_EC_CODGRUPPO\" = '" + VarArticoli[10] + "' ");
            oRecordset2.DoQuery("SELECT \"Code\" FROM \"@FO_EC_COLORI_FILATO\" WHERE \"Code\" = '" + VarArticoli[10] + "' ");
            if (oRecordset2.RecordCount == 0)
            {
                error = 1;
                Log("ERRORE, colore " + VarArticoli[10] + " non trovato nella tabella dei colori, articolo non aggiornato");
            }
            else
            {
                oItem.UserFields.Fields.Item("U_FO_EC_COLORI").Value = VarArticoli[10];
            }

            oRecordset2.DoQuery("SELECT \"Code\" FROM \"@FO_EC_TAGLIA\" WHERE \"Name\" LIKE '" + VarArticoli[11] + "'");
            if (oRecordset2.RecordCount > 0)
            {
                oItem.UserFields.Fields.Item("U_FO_EC_TAGLIA").Value = oRecordset2.Fields.Item(0).Value.ToString();
            }
            else
            {
                error = 1;
                Log("ERRORE, taglia " + VarArticoli[11] + " non trovata nella tabella delle taglie, articolo non aggiornato");
            }

            oItem.User_Text = VarArticoli[13];

            oRecordset2.DoQuery("SELECT \"U_FO_EC_COPOSIZIONE\" FROM \"@FO_EC_COMPOSIZIONI\" WHERE \"U_FO_EC_COPOSIZIONE\" = '" + VarArticoli[14] + "'");
            if (oRecordset2.RecordCount > 0)
            {
                oItem.UserFields.Fields.Item("U_FO_EC_COMPOSIZIONE").Value = VarArticoli[14].ToString();
            }

            oRecordset2.DoQuery("SELECT \"Code\" FROM \"@FO_EC_COLLEZIONI\" WHERE \"Name\" LIKE '" + VarArticoli[17] + "'");
            if (oRecordset2.RecordCount > 0)
            {
                oItem.UserFields.Fields.Item("U_FO_EC_COL1").Value = oRecordset2.Fields.Item(0).Value.ToString();
            }

            oRecordset2.DoQuery("SELECT \"Code\" FROM \"@FO_EC_LAVAGGIO\" WHERE \"Name\" = '" + VarArticoli[15] + "'");
            if (oRecordset2.RecordCount > 0)
            {
                oItem.UserFields.Fields.Item("U_FO_EC_LAVAGGIO").Value = oRecordset2.Fields.Item(0).Value.ToString();
            }

            if (VarArticoli[16] != "")
            {
                //Converto il peso da kg a g
                double Peso = double.Parse(VarArticoli[16]) * 1000;
                oItem.UserFields.Fields.Item("U_FO_EC_PESO").Value = Peso;
                if (Peso >= 1000)
                {
                    oItem.PurchaseUnitWeight = Peso;
                    oItem.SalesUnitWeight = Peso;
                }
                else
                {
                    oItem.PurchaseUnitWeight = 1000;
                    oItem.SalesUnitWeight = 1000;
                }

            }

            if (Nuovo)
            {
                if (error == 0)
                {
                    res = oItem.Add();
                    if (res != 0)
                    {
                        string err;
                        int code;
                        oCompany.GetLastError(out code, out err);
                        Log("ERRORE inserimento articolo " + ItemCode + " errore n: " + code + ", " + err);
                    }
                    else
                    {
                        Log("Inserito articolo " + ItemCode);
                    }
                }
                else
                {
                    res = 1;
                }
            }
            else
            {
                if (error == 0)
                {
                    res = oItem.Update();
                    if (res != 0)
                    {
                        string err;
                        int code;
                        oCompany.GetLastError(out code, out err);
                        Log("ERRORE aggiornamento articolo " + ItemCode + " errore n: " + code + ", " + err);
                    }
                    else
                    {
                        Log("Aggiornato articolo " + ItemCode);
                    }
                }
                else
                {
                    res = 1;
                }
            }
            return res;
        }

        private int CreaEntrataMerci(ArrayList Articoli, ArrayList QtaArt, ArrayList MagazzinoArt)
        {
            if (Articoli.Count == 0) return 0;

            int res = 0;
            int error = 0;
            //oOIGN = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);
            //oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            //oRecordset2 = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            //oOIGN.DocDate = DateTime.Now;
            //oOIGN.DocDueDate = DateTime.Now;
            //int index = 0;
            //foreach (string ItemCode in Articoli)
            //{
            //    oOIGN.Lines.ItemCode = ItemCode;
            //    oOIGN.Lines.Quantity = double.Parse(QtaArt[index].ToString().Replace(".", ","));
            //    oOIGN.Lines.Add();
            //    index++;
            //}
            //oOIGN.Comments = "Entrata merci relativa all'importazione articoli di " + DateTime.Now.ToLongDateString();
            //res = oOIGN.Add();

            oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordset.DoQuery("SELECT \"Name\" FROM \"@ADDONPAR\" WHERE \"Code\" = 'MAGAZZINO_INVENTARIO'");

            try
            {
                SAPbobsCOM.CompanyService oCS = oCompany.GetCompanyService();
                SAPbobsCOM.InventoryPostingsService oICS = oCS.GetBusinessService(SAPbobsCOM.ServiceTypes.InventoryPostingsService);
                SAPbobsCOM.InventoryPosting oIC = oICS.GetDataInterface(SAPbobsCOM.InventoryPostingsServiceDataInterfaces.ipsInventoryPosting) as SAPbobsCOM.InventoryPosting;
                oIC.CountDate = DateTime.Now;
                SAPbobsCOM.InventoryPostingLines oICLS = oIC.InventoryPostingLines;
                oIC.PriceSource = SAPbobsCOM.InventoryPostingPriceSourceEnum.ippsLastEvaluatedPrice;

                int index = 0;
                foreach (string ItemCode in Articoli)
                {
                    SAPbobsCOM.InventoryPostingLine oICL = oICLS.Add();
                    oICL.ItemCode = ItemCode;
                    oICL.CountedQuantity = double.Parse(QtaArt[index].ToString().Replace(".", ","));
                    if (MagazzinoArt[index].ToString() != null && MagazzinoArt[index].ToString() != "")
                    {
                        oICL.WarehouseCode = MagazzinoArt[index].ToString();
                    }
                    else
                    {
                        oICL.WarehouseCode = oRecordset.Fields.Item(0).Value.ToString();
                    }
                    
                    oICL.Price = 0.0;
                    //oICL.Counted = SAPbobsCOM.BoYesNoEnum.tYES;
                    index++;
                }

                SAPbobsCOM.InventoryPostingParams oICP = oICS.Add(oIC);
            }
            catch (Exception e)
            {
                string err;
                int code;
                oCompany.GetLastError(out code, out err);
                Log("ERRORE aggiornamento inventario errore n: " + code + ", " + err + " -- " + e.ToString());
                res = 1;
                throw;
            }

            if (res == 0)
            {
                Log("Aggiornamento Inventario eseguito con successo ");
            }
            return res;
        }

        private int CreaListino(string[] VarListini)
        {
            int res = 0;
            oSPE = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oSpecialPrices);
            oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordset2 = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            string ItemCode = VarListini[1];
            string CardCode = VarListini[0];


            oRecordset.DoQuery("SELECT \"ItemCode\" FROM OSPP WHERE \"ItemCode\" = '" + ItemCode + "' AND \"CardCode\" = '" + CardCode + "'");
            if (oRecordset.RecordCount == 0)
            {
                oSPE.ItemCode = ItemCode;
                oSPE.CardCode = CardCode;
                if (VarListini[4].ToString() =="")
                {
                    oSPE.Price = double.Parse(VarListini[2].Replace(".", ","));
                }
                else
                {
                    oSPE.Price = double.Parse(VarListini[4].Replace(".", ","));
                }
                oSPE.Currency = VarListini[3];
                if (VarListini[4].ToString() != "")
                {
                    oSPE.SpecialPricesDataAreas.DateFrom = DateTime.Now.Date;
                    oSPE.SpecialPricesDataAreas.SpecialPrice = double.Parse(VarListini[2].Replace(".", ","));
                    //oSPE.SpecialPricesDataAreas.Add();
                }
                res = oSPE.Add();
                if (res != 0)
                {
                    string err;
                    int code;
                    oCompany.GetLastError(out code, out err);
                    Log("ERRORE inserimento listino per articolo " + ItemCode + " e BP : " + CardCode + " errore n: " + code + ", " + err);
                }
                else
                {
                    Log("Inserito listino per articolo " + ItemCode + " e BP " + CardCode);
                }
            }
            else
            {
                bool ADDSpecialPricesDataAreas = true;
                oSPE.GetByKey(ItemCode, CardCode);
                if (VarListini[4].ToString() == "")
                {
                    oSPE.Price = double.Parse(VarListini[2].Replace(".", ","));
                }
                else
                {
                    oSPE.Price = double.Parse(VarListini[4].Replace(".", ","));
                }
                oSPE.Currency = VarListini[3];

                if (VarListini[4].ToString() != "")
                {

                    ///
                    int count = oSPE.SpecialPricesDataAreas.Count;
                    for (int a = 0; a < count; a++)
                    {
                        oSPE.SpecialPricesDataAreas.SetCurrentLine(a);
                        if (oSPE.SpecialPricesDataAreas.DateFrom == DateTime.Now.Date)
                        {
                            ADDSpecialPricesDataAreas = false;
                            oSPE.SpecialPricesDataAreas.SpecialPrice = double.Parse(VarListini[2].Replace(".", ","));
                        }
                    }
                    if (ADDSpecialPricesDataAreas)
                    {
                        oSPE.SpecialPricesDataAreas.Add();
                        oSPE.SpecialPricesDataAreas.DateFrom = DateTime.Now.Date;
                        oSPE.SpecialPricesDataAreas.SpecialPrice = double.Parse(VarListini[2].Replace(".", ","));
                        //oSPE.SpecialPricesDataAreas.Add();
                    }
                }
                ///

                res = oSPE.Update();
                if (res != 0)
                {
                    string err;
                    int code;
                    oCompany.GetLastError(out code, out err);
                    Log("ERRORE aggiornamento listino per articolo " + ItemCode + " e BP : " + CardCode + " errore n: " + code + ", " + err);
                }
                else
                {
                    Log("Aggiornato listino per articolo " + ItemCode + " e BP " + CardCode);
                }
            }
            return res;
        }


        /* EXPORT .CSV Articoli*/
        private void GenFileExport(string PathDest)
        {
            SAPbobsCOM.Recordset oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            string q = "SELECT 'Codice EC' ,'Stagione di vendita', 'Stagione del modello',";
            q = q + " 'Identificativo SAP', 'Codice a barre', 'Marchio', 'Linea' , 'Settore', 'Modello', 'Tessuto', 'Drop' ,";
            q = q + " 'Colore' , 'Taglia', 'Nome per sito' , 'Descrizione prodotto', 'Composizione', 'Lavaggio', 'Quantità',";
            q = q + "'Prezzo','Valuta','Bozza','Peso','Collezione','Codice doganale'  FROM DUMMY ";
            q = q + "UNION ";
            q = q + "SELECT T5.\"CardCode\" AS \"Codice EC\", T0.U_FO_STAGIONE_VENDITA AS \"Stagione di Vendita\", T0.U_FO_STAGIONE_SVILUPPO AS \"Stagione del Modello\", ";
            q = q + "T0.\"ItemCode\" AS \"Identificativo SAP\" , T0.\"CodeBars\" AS \"Codice a barre\", T0.\"U_FO_MARCHIO\" AS \"Marchio\",  ";
            q = q + "T0.\"U_FO_LINEA\" AS \"Linea\" , T0.\"U_FO_SETTORE\" AS \"Settore\", T0.\"U_FO_EC_SKU\" AS \"Modello\", T0.\"U_FO_TESSUTO\" AS \"Tessuto\", ";
            q = q + "T0.\"U_FO_EC_DROP\" AS \"Drop\" , T0.\"U_FO_EC_COLORI\" AS \"Colore\", T11.\"Name\" AS \"Taglia\", "; //NB taglia
            q = q + "T0.\"ItemName\" AS \"Nome per sito\", TO_VARCHAR(T0.\"UserText\") AS \"Descrizione Prodotto\", T0.\"U_FO_EC_COMPOSIZIONE\" AS \"Composizione\", ";
            q = q + "T0.\"U_FO_EC_LAVAGGIO\" AS \"Lavaggio\" , Replace(T0.\"OnHand\",'.',',') AS \"Quantità\", ";
            q = q + "Replace( CASE WHEN(T10.\"Price\"= 0) THEN COALESCE(T6.\"Price\",T5.\"Price\") ";
            q = q + "WHEN(T10.\"Price\" IS NULL) THEN COALESCE(T6.\"Price\",T5.\"Price\") ";
            q = q + "WHEN(T10.\"Price\" IS NOT NULL AND T5.\"AutoUpdt\"= 'N') THEN COALESCE(T6.\"Price\" ,T5.\"Price\") ";
            q = q + "ELSE T10.\"Price\" ";
            q = q + "END ,'.',',') AS \"Prezzo\" ,";
            q = q + "Replace( CASE WHEN(T10.\"Price\"= 0) THEN COALESCE(T6.\"Currency\",T5.\"Currency\") ";
            q = q + "WHEN(T10.\"Price\" IS NULL) THEN COALESCE(T6.\"Currency\", T5.\"Currency\") ";
            q = q + "WHEN(T10.\"Price\" IS NOT NULL AND T5.\"AutoUpdt\"= 'N') THEN COALESCE(T6.\"Currency\", T5.\"Currency\") ";
            q = q + "ELSE T10.\"Currency\" ";
            q = q + "END  ,'.',',') AS \"Valuta\" , ";
            q = q + " 'No' AS \"Bozza\", Replace(T0.\"U_FO_EC_PESO\" ,'.',',') AS \"Peso\", ";
            q = q + "T12.\"Name\" AS \"Collezione\" , T34.\"Code\" AS \"Codice Doganale\" ";
            q = q + "FROM OITM T0 INNER JOIN ITM10 T1 ON T0.\"ItemCode\" = T1.\"ItemCode\" ";
            q = q + "LEFT OUTER JOIN ODCI T34 ON T1.\"ISCommCode\" = T34.\"AbsEntry\" AND T34.\"ConfType\" = 'C'";
            q = q + "LEFT OUTER JOIN \"@FO_EC_TAGLIA\" T11 ON T11.\"Code\" = T0.\"U_FO_EC_TAGLIA\" ";
            q = q + "LEFT OUTER JOIN \"@FO_EC_COLLEZIONI\" T12 ON T12.\"Code\" = T0.\"U_FO_EC_COL1\" ";
            q = q + "INNER JOIN \"@FO_EC_ARTICOLI\" T3 ON T0.\"ItemCode\"= T3.\"U_FO_EC_ITEMCODE\" ";
            //q = q + "--INNER JOIN OITM T1 ON T0.U_FO_EC_SKU = T1.U_FO_EC_SKU";
            q = q + "LEFT OUTER JOIN OSCN T2 ON T3.U_FO_EC_ITEMCODE = T2.\"ItemCode\" AND T3.U_FO_EC_CARDCODE = T2.\"CardCode\" ";
            q = q + "INNER JOIN OSPP T5 ON T5.\"ItemCode\" = T3.U_FO_EC_ITEMCODE AND T5.\"CardCode\" = T3.U_FO_EC_CARDCODE ";
            q = q + "LEFT OUTER JOIN SPP1 T6 ON T5.\"CardCode\" = T6.\"CardCode\" AND T6.\"ItemCode\" = T5.\"ItemCode\" AND ";
            //q = q + "GETDATE() BETWEEN T6.\"FromDate\" AND COALESCE(T6.\"ToDate\", GETDATE()) ";
            q = q + "current_date BETWEEN T6.\"FromDate\" AND COALESCE(T6.\"ToDate\", current_date) ";
            q = q + "LEFT OUTER JOIN ITM1 T10 ON T10.\"ItemCode\" = T5.\"ItemCode\" and T5.\"ListNum\" = T10.\"PriceList\" ";
            q = q + "WHERE T5.\"CardCode\" IN (select \"U_FO_EC_CARDCODE\" from \"@FO_EC_DETNEGOZIO\" ) ";
            q = q + " order by 1 ";
            oRecordset.DoQuery(q);
            /*while (!oRecordset.EoF)
            {
                if (oRecordset.Fields.Item(20).Value == 'Y')
                {
                    oRecordset.Fields.Item(20).Value = "No";
                }
                else
                {
                    oRecordset.Fields.Item(20).Value = "Si";
                }
                oRecordset.MoveNext();
            }*/
            string NomeOutput = "ExportArticoli_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            string PathFileXML = "C:/temp/" + NomeOutput + ".xml";
            string PathFileTxt = PathDest + @"\" + NomeOutput;
            oRecordset.SaveToFile(PathFileXML);
            ConvertiXML(PathFileXML, PathFileTxt, NomeOutput, ".csv");
        }

        private void ConvertiXML(string PathXml, string PathFileTXT, string Query, string FormatoFile)
        {
            string ContenutoRiga = "";
            string ContenutoPulito = "";
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(PathXml);
            //Mi posiziono sull'elemento ROW e ciclo 
            var riga = xdoc.GetElementsByTagName("row");
            for (int i = 0; i < riga.Count; i++)
            {
                ContenutoRiga = "";
                foreach (XmlNode node in riga[i].ChildNodes)
                {
                    ContenutoRiga += string.Format("{0};", node.InnerText);
                }
                string FineRiga = ContenutoRiga.Remove(ContenutoRiga.Length - 1);
                ContenutoPulito = FineRiga + " ; ";
                //Scrivo su un file di testo passandoli il contenuto e il file
                WriteFile(FineRiga, PathFileTXT, FormatoFile);
            }

            //Cancella XML 
            File.Delete(PathXml);
        }

        private void WriteFile(string Testo, string PathFileTxt, string FormatoFile)
        {
            DateTime dt = new DateTime();
            dt = DateTime.Now;
            string FileOut = PathFileTxt + FormatoFile;
            using (StreamWriter w = File.AppendText(FileOut))
            {
                w.WriteLine(Testo);
            }
        }

    }
}
